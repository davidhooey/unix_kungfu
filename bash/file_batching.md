# File Batching

-------------------------------------------------------------------------------
Extract all `tar.gz` files into their own directory using the base file name as
the directory name.

```bash
for f in *.tar.gz; do mkdir "${f%%.*}"; tar xvf "${f}" -C "${f%%.*}"; done
```
-------------------------------------------------------------------------------
Purge all `linux-headers-*` from `/usr/src` which do not match the current
kernel verison. Run within the `/usr/src` directory as `root`.

```bash
for d in *; do if [[ "$d" != "linux-headers-$(uname -r | cut -d '-' -f 1,2)"* ]]; then apt-get purge -y "${d}"; fi; done
```
-------------------------------------------------------------------------------
