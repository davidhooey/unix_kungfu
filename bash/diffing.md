# Diffing

-------------------------------------------------------------------------------
Compare folder structure without comparing file contents.

```bash
find /directory1 -printf "%P\n" | sort > directory1.out
find /directory2 -printf "%P\n" | sort | diff - directory1.out

# If `tree` is available.
tree /directory1 > directory1.out
tree /directory2 > directory2.out
diff directory1.out directory2.out
```
-------------------------------------------------------------------------------
