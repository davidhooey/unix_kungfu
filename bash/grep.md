# Grep

-------------------------------------------------------------------------------
Find the last occurance from each file.

```bash
for name in filename*; do grep -H search_term "$name" | tail -n 1; done;
```
-------------------------------------------------------------------------------
